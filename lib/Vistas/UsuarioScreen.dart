import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:path/path.dart';
import 'package:proyecto_final_software_flutter/Vistas/Modificar.dart';

class UsuarioScreen extends StatefulWidget{

  final String nickName;
  final String ci;
  final String carrera;
  final String foto_perfil;
  final String nombre;

  UsuarioScreen({this.nickName, this.ci, this.carrera, this.foto_perfil,  this.nombre});
  
  @override
    _UsuarioScreenState createState() => new _UsuarioScreenState();
}

  class _UsuarioScreenState extends State<UsuarioScreen> {
  @override
    Widget build(BuildContext context) {
      // TODO: implement build
      return new Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          title: new Text("Perfil", style: TextStyle(color: Colors.grey)),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.grey),
          
        ),
        body: 
        SingleChildScrollView(
        child: new Container(
          child: new Column(
            children: <Widget>[
              
              new Divider(height: 1.0),
               new Container(
                              padding: const EdgeInsets.only(top:10.0,left: 20.0,right: 10.0,bottom: 15.0),
                              child: Text("Nombre: "+widget.nickName, style: TextStyle(fontSize: 20.0, color: Colors.black),),
                            ),
              
              new Container(
                child: new Image(
                          image: NetworkImage(widget.foto_perfil),
                          fit: BoxFit.fill,
                          height: 330,
                          width: double.infinity,
                          
                        ),
                      
                  padding: const EdgeInsets.all(0.0), 
            
              ),
              //Text(widget.titulo, style: TextStyle(fontSize: 22.0, color: Colors.black),),

              new Row(
                          //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            new Container(
                              padding: const EdgeInsets.only(left: 20.0, top: 15.0),
                              child: CircleAvatar(
                                radius: 20.0,
                                backgroundColor: const Color(0xFF778899),
                                backgroundImage: NetworkImage(widget.foto_perfil),
                              ),
                            ),

                            Padding(padding: const EdgeInsets.only(left: 5.0),),
                            new Column(
                              children: <Widget>[
                                Text(
                                  "Carrera: "+widget.carrera,
                                  style: TextStyle(fontSize: 18.0, color: Colors.black),
                                ),
                              ],
                            )
                          ],
                        ),

                          new Container(
                              padding: const EdgeInsets.only(top:25.0,left: 20.0,right: 20.0,bottom: 15.0),
                              child: Text("Carnet de Identidad: "+widget.ci,
                              style: TextStyle(fontSize: 15.0, color: Colors.black, ),textAlign: TextAlign.justify)
                            ),

                            new Container(
                              padding: const EdgeInsets.only(top:25.0,left: 20.0,right: 20.0,bottom: 15.0),
                              child: Text(widget.nombre,
                              style: TextStyle(fontSize: 20.0, color: Colors.black, ),textAlign: TextAlign.justify)
                            ),

                            new Container(
                              padding: const EdgeInsets.only(top:25.0,left: 20.0,right: 20.0,bottom: 15.0),
                              child:new MaterialButton(
                            minWidth: MediaQuery.of(context).size.width,
                            onPressed: () {
                               var route=new MaterialPageRoute(
                                          builder:(BuildContext context)=> new Modificar(
                                            nickName: widget.nickName,
                                            ci: widget.ci,
                                            carrera: widget.carrera,
                                            foto_perfil: widget.foto_perfil,
                                            nombre: widget.nombre,
                                          )
                                          
                                  );
                                  Navigator.of(context).push(route);
                              },
                            child: Text("Modificar",
                                textAlign: TextAlign.center,
                                ),
                          ),
                            ),
              
            ],
          ),
        ),
        )
      );
    }

    }