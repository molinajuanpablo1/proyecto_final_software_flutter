import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:path/path.dart';
import 'package:proyecto_final_software_flutter/APIs/UsuariosAPIs.dart';
import 'package:proyecto_final_software_flutter/Vistas/Listado.dart';
import 'package:proyecto_final_software_flutter/Vistas/Login.dart';

const double _kPickerSheetHeight = 216.0;
const double _kPickerItemHeight = 40.0;

const List<String> carreraSelect = <String>[
  'Administración de Empresas', 'Administración Turística', 'Arquitectura', 'Comunicación Social', 'Ciencias Políticas',
  'Contaduria Pública', 'Derecho', 'Diseño Grafico y Comunicación Visual', 'Economia', 'Ingeniería Ambiental', 'Ingeniería Biomédica',
  'Ingeniería Civil', 'Ingeniería Comercial', 'Ingeniería de Sistemas', 'Ingeniería de Telecomunicaciones', 'Ingeniería Industrial',
  'Ingeniería Mecatrónica', 'Ingeniería Química', 'Psicologia', 'Psicopedagogía',
];

class Modificar extends StatefulWidget{

  final String nickName;
  final String ci;
  final String carrera;
  final String foto_perfil;
  final String nombre;
  
  static const String routeName = '/cupertino/picker';

  Modificar({this.nickName, this.ci, this.carrera, this.foto_perfil,  this.nombre});
  
  @override
    _ModificarState createState() => new _ModificarState();
}

  class _ModificarState extends State<Modificar> {
    TextEditingController ciController ;
    TextEditingController nickNameController ;
    TextEditingController carreraController ;
    Future _usuarioAPI;
    int _selectedMajorIndex = 0 ;

    void initState() {
      ciController= new TextEditingController(text: widget.ci );
      nickNameController= new TextEditingController(text: widget.nickName);
      carreraController= new TextEditingController( text: widget.carrera );
      super.initState();
      }
    
  @override
    Widget build(BuildContext context) {
      // TODO: implement build
      return new Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          title: new Text("Modificacion del Perfil", style: TextStyle(color: Colors.grey)),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.grey),
          
        ),
        body: 
        SingleChildScrollView(
        child: new Container(
          child: new Column(
            children: <Widget>[
              
              new Divider(height: 1.0),
               new Container(
                              padding: const EdgeInsets.only(top:10.0,left: 20.0,right: 10.0,bottom: 15.0),
                              child: TextField(controller: nickNameController,
                                        ),
                            ),

                new Container(
                              padding: const EdgeInsets.only(top:10.0,left: 20.0,right: 10.0,bottom: 15.0),
                              child: TextField(controller: carreraController,
                                        ),
                            ),
                           // _buildMajorPicker(context),
                            

              new Container(
                              padding: const EdgeInsets.only(top:25.0,left: 20.0,right: 20.0,bottom: 15.0),
                              child:new MaterialButton(
                              minWidth: MediaQuery.of(context).size.width,
                              onPressed: () {
                                Navigator.of(context).pop();
                                  setState(() {
                                            _usuarioAPI = new UsuariosAPIs().updateUser(ciController.text, nickNameController.text, carreraController.text);
                                            Navigator.push(context,
                                              MaterialPageRoute(
                                              builder: (context)=> Listado()
                                              )
                                              );
                                          });
                              },
                              child: Text("Modificar",
                                  textAlign: TextAlign.center,
                                  ),
                            ),
                            ),
            ],
          ),
        ),
        )
      );
    }

       Widget _buildMajorPicker(BuildContext context) {
    final FixedExtentScrollController carreraController =
    FixedExtentScrollController(initialItem: _selectedMajorIndex);

    return GestureDetector(
      onTap: () async {
        await showCupertinoModalPopup<void>(
          context: context,
          builder: (BuildContext context) {
            return _buildBottomPicker(
              CupertinoPicker(
                scrollController: carreraController,
                magnification:1.0,
                itemExtent: _kPickerItemHeight,
                backgroundColor: CupertinoColors.white,
                useMagnifier:true,
                onSelectedItemChanged: (int index) {
                  if (mounted) {
                    setState(() => _selectedMajorIndex = index);
                  }
                },
                children: List<Widget>.generate(carreraSelect.length, (int index) {
                  return Center(child:
                    Text(carreraSelect[index]),
                  );
                }),
              ),
            );
          },
        );
      },
      child: _buildMenu(
        <Widget>[
          const Text('Carrera: ',
            style: TextStyle(color: Colors.grey,
              fontSize: 16.0,
            ),),
          Text(
            carreraSelect[_selectedMajorIndex],
            style: const TextStyle(
                color: CupertinoColors.black 
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBottomPicker(Widget picker) {
    return Container(
      height: _kPickerSheetHeight,
      padding: const EdgeInsets.only(top: 6.0),
      color: CupertinoColors.white,
      child: DefaultTextStyle(
        style: const TextStyle(
          color: CupertinoColors.black,
          fontSize: 22.0,
        ),
        child: GestureDetector(
          onTap: () {},
          child: SafeArea(
            top: false,
            child: picker,
          ),
        ),
      ),
    );
  }
  Widget _buildMenu(List<Widget> children) {
    return Container(
      decoration: BoxDecoration(
        color:CupertinoColors.white, 
        border: const Border(
          top: BorderSide(color: Color(0xFFBCBBC1), width: 0.0),
          bottom: BorderSide(color: Color(0xFFBCBBC1), width: 0.0),
        ),
      ),
      height: 44.0,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 2.0),
        child: SafeArea(
          top: false,
          bottom: false,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: children,
          ),
        ),
      ),
    );
  }


    }