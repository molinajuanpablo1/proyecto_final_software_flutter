import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proyecto_final_software_flutter/DTO/UsuarioDTO.dart';
import 'package:proyecto_final_software_flutter/APIs/UsuariosAPIs.dart';
import 'dart:async';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:proyecto_final_software_flutter/Vistas/Login.dart';
import 'package:proyecto_final_software_flutter/Vistas/UsuarioScreen.dart';

class Listado extends StatefulWidget{
  @override
  _Lista createState() => _Lista();
  
  }
  
  class _Lista extends State<Listado> {
    TextEditingController buscador;
    Future _usuarioAPI;
    //FocusNode _searchFocus;
    
    @override
  void initState() {
    super.initState();
    buscador = new TextEditingController();
    _usuarioAPI = new UsuariosAPIs().getUsers('');
  }
 /* @override
  void dispose() {
    super.dispose();
    _searchFocus.dispose();
  }*/

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: new AppBar(
        title: new Text("Usuarios", style: TextStyle(color: Colors.grey),),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.grey),
          
      ),
      body: Padding(
        padding: EdgeInsets.all(5),
        child: Column(
          children: <Widget>[
            
            Flexible(
              child: FutureBuilder<List<UsuarioDTO>>(
                future: _usuarioAPI,
                builder: (context, snapshot) {
                  if(snapshot.connectionState == ConnectionState.done) {
                    if(snapshot.hasError){
                      return Text("Ha ocurrido un error");
                    }
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index){
                        return new Container(
                          child: new Card(
                            color: Colors.white,
                            child: new InkWell(
                              onTap: () {
                                var route=new MaterialPageRoute(
                                          builder:(BuildContext context)=> new UsuarioScreen(
                                            nickName: snapshot.data[index].nickName,
                                            ci: snapshot.data[index].ci,
                                            carrera: snapshot.data[index].carrera,
                                            foto_perfil: snapshot.data[index].foto_perfil,
                                            nombre: snapshot.data[index].nombre,
                                          )
                                          
                                  );
                                  Navigator.of(context).push(route);
                              },
                              onLongPress: (){
                                showDialog(
                                context: context,
                                builder: (BuildContext context) => _buildAboutDialog(context, snapshot.data[index].ci, snapshot.data[index].nickName),
                              );
                              },
                              child: Row(
                                children: <Widget>[
                                  Container(
                                      child:
                                        Image(
                                          image: NetworkImage(snapshot.data[index].foto_perfil),
                                          fit: BoxFit.fill,
                                          height: 100,
                                          width: 100,

                                        ),
                                  ),
                                  Flexible(
                                    child: 
                                    Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                        new Container(
                                                  padding: const EdgeInsets.only(top:2.0,left: 5.0,right: 5.0,bottom: 2.0),
                                                  child: Text(snapshot.data[index].nickName, overflow: TextOverflow.ellipsis, maxLines: 1,
                                                  style: TextStyle(color: Colors.black, fontSize: 18), textAlign: TextAlign.center, ),
                                                ),
                                        
                                        
                                      Row(children: <Widget>[ 
                                        
                                        new Container(
                                                  padding: const EdgeInsets.only(top:2.0,left: 5.0,right: 5.0,bottom: 2.0),
                                                  child: Text(snapshot.data[index].carrera, overflow: TextOverflow.ellipsis, maxLines: 1, 
                                                  style: TextStyle(color: Colors.black, fontSize: 13
                                                ), 
                                                  textAlign: TextAlign.justify
                                                ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }
                  else
                    return CircularProgressIndicator();
                }
            ),
            ),
          ],
        )
      )
    );
  }


  Widget _buildAboutDialog(BuildContext context, String ci, String nickName) {
    print(ci);
    return new AlertDialog(
      title: new Text("Se borrara al ususario con CI: "+ ci),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildAboutText(context, ci, nickName),
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
           Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child:  Text('Cancelar' , textAlign: TextAlign.left,),
        ),
        new FlatButton(

          onPressed: () {
           Navigator.of(context).pop();
           setState(() {
                    _usuarioAPI = new UsuariosAPIs().deleteUser(ci);
                    Navigator.push(context,
                      MaterialPageRoute(
                      builder: (context)=> Listado()
                      )
                      );
                  });
            MaterialPageRoute(
              
            builder: (context)=> Login()
            );
          },
          textColor: Theme.of(context).primaryColor,
          child:  Text('Borrar al usuario'),
        ),
      ],
    );
  }

Widget _buildAboutText(BuildContext context, String ci, String nickName) {
    return new RichText(
      text: new TextSpan(
        text: 'Se eliminara al usuario $nickName con carnet: $ci permanentemente ¿está seguro de realizar esta acción?\n\n',
        style: const TextStyle(color: Colors.black87),
      ),
    );
  }

  }