import 'package:flutter/material.dart';
import 'package:proyecto_final_software_flutter/APIs/UsuariosAPIs.dart';
import 'package:proyecto_final_software_flutter/Vistas/Login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:proyecto_final_software_flutter/APIs/AuthAPIs.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => new _RegisterState();
}


class _RegisterState extends State<Register> {
  bool _validate1=true;
  TextEditingController ciController ;
  TextEditingController passController ;
  TextEditingController nickNameController ;
  TextEditingController carreraController ;
  TextEditingController apellido_pController ;
  TextEditingController apellido_mController ;
  TextEditingController emailController ;
  TextEditingController nombreController ;
  TextEditingController rolController ;

  String msg;
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 10.0);
  Future _usuarioAPI;

  @override
  void initState() {
    super.initState();
    msg = 'Inexistente';
    _validate1 = true;
    ciController = new TextEditingController();
    passController = new TextEditingController();
    nickNameController = new TextEditingController();
    carreraController = new TextEditingController();
    apellido_pController = new TextEditingController();
    apellido_mController = new TextEditingController();
    emailController = new TextEditingController();
    nombreController = new TextEditingController();
    rolController = new TextEditingController();
  }
  Widget build(BuildContext context) {
    
    return  Scaffold(
      appBar: AppBar(
        title: new Text("Registrar", style: TextStyle(color: Colors.grey),),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.grey),
        ),
      body: new Container(
        decoration: new BoxDecoration(
                    gradient: new LinearGradient(colors: [const Color(0xFF252D4F),const Color(0xFF9BDBDD)],
                        begin: FractionalOffset.topCenter,
                        end: FractionalOffset.bottomCenter,
                        stops: [0.0,1.0],
                        tileMode: TileMode.clamp
                    ),
                  ),

        padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        child: new LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constrains){
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: constrains.maxHeight
                  ),
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        new Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: new TextField(
                            controller: ciController,
                            style: new TextStyle(
                              fontSize: 15.0,
                              height: 1.0,
                              color: Colors.black                  
                            ),
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                hintText: "Carnet de Identidad",
                                errorText: _validate1 ? null: 'Ingresar el carnet de identidad',
                                border:
                                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                          ),
                        ),

                        new Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: new TextField(
                            controller: passController,
                            obscureText: true,
                            style: new TextStyle(
                              fontSize: 15.0,
                              height: 1.0,
                              color: Colors.black                  
                            ),
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                hintText: "Password",
                                errorText: _validate1 ? null : 'Ingresar la contraseña',
                                border:
                                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                          ),
                        ),

                        new Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: new TextField(
                            controller: nickNameController,
                            style: new TextStyle(
                              fontSize: 15.0,
                              height: 1.0,
                              color: Colors.black                  
                            ),
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                hintText: "nick Name",
                                errorText: _validate1 ? null : 'Ingresar el NIck Name',
                                border:
                                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                          ),
                        ),
                        new Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: new TextField(
                            controller: carreraController,
                            style: new TextStyle(
                              fontSize: 15.0,
                              height: 1.0,
                              color: Colors.black                  
                            ),
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                hintText: "Carrera",
                                errorText: _validate1 ? null : 'Ingresar la Carrera',
                                border:
                                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                          ),
                        ),
                        new Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: new TextField(
                            controller: apellido_pController,
                            style: new TextStyle(
                              fontSize: 15.0,
                              height: 1.0,
                              color: Colors.black                  
                            ),
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                hintText: "Apellido Paterno",
                                errorText: _validate1 ? null : 'Ingresar el Apellido Paterno',
                                border:
                                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                          ),
                        ),
                        new Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: new TextField(
                            controller: apellido_mController,
                            style: new TextStyle(
                              fontSize: 15.0,
                              height: 1.0,
                              color: Colors.black                  
                            ),
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                hintText: "Apellido Materno",
                                errorText: _validate1 ? null : 'Ingresar el Apellido Materno',
                                border:
                                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                          ),
                        ),
                        new Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: new TextField(
                            controller: emailController,
                            style: new TextStyle(
                              fontSize: 15.0,
                              height: 1.0,
                              color: Colors.black                  
                            ),
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                hintText: "Email",
                                errorText: _validate1 ? null : 'Ingresar el email',
                                border:
                                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                          ),
                        ),

                        new Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: new TextField(
                            controller: nombreController,
                            style: new TextStyle(
                              fontSize: 15.0,
                              height: 1.0,
                              color: Colors.black                  
                            ),
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                hintText: "Nombre",
                                errorText: _validate1 ? null : 'Ingresar el nombre',
                                border:
                                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                          ),
                        ),

                        new Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: new TextField(
                            controller: rolController,
                            style: new TextStyle(
                              fontSize: 15.0,
                              height: 1.0,
                              color: Colors.black                  
                            ),
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                hintText: "Rol",
                                errorText: _validate1 ? null : 'Ingresar el rol',
                                border:
                                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                          ),
                        ),

                        new Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: new MaterialButton(
                            minWidth: MediaQuery.of(context).size.width,
                            onPressed: () {
                                Navigator.of(context).pop();
                                  setState(() {
                                            _usuarioAPI = new UsuariosAPIs().createUser(ciController.text, passController.text, nickNameController.text, carreraController.text,
                                                                       apellido_pController.text, apellido_mController.text, emailController.text, nombreController.text, rolController.text);
                                            Navigator.push(context,
                                              MaterialPageRoute(
                                              builder: (context)=> Login()
                                              )
                                              );
                                          });
                              },
                            child: Text("Registrar",
                                textAlign: TextAlign.center,
                                style: new TextStyle(
                                        fontSize: 15.0,
                                        height: 1.0,
                                        color: Colors.black38                  
                                      ),
                                    ),
                          ),
                        ),

                      ],
                    )),
                ),
              );
          }
        ),

      ),
    );
  }

    
}