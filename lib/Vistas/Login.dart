import 'package:flutter/material.dart';
import 'package:proyecto_final_software_flutter/APIs/UsuariosAPIs.dart';
import 'package:proyecto_final_software_flutter/Vistas/Register.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:proyecto_final_software_flutter/APIs/AuthAPIs.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => new _LoginState();
}


class _LoginState extends State<Login> {
  bool _validate1=true;
  TextEditingController ciController ;
  TextEditingController passController ;
  String msg;
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  Future _usuarioAPI;

  @override
  void initState() {
    super.initState();
    getToken();
    msg = 'Inexistente';
    _validate1 = true;
    ciController = new TextEditingController();
    passController = new TextEditingController();
    _usuarioAPI = new UsuariosAPIs().getUsers('');
  }
  Widget build(BuildContext context) {
    
    return  Scaffold(
      appBar: AppBar(
        title: new Text("Login", style: TextStyle(color: Colors.grey),),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.grey),
        ),
      body: new Container(
        decoration: new BoxDecoration(
                    gradient: new LinearGradient(colors: [const Color(0xFF252D4F),const Color(0xFF9BDBDD)],
                        begin: FractionalOffset.topCenter,
                        end: FractionalOffset.bottomCenter,
                        stops: [0.0,1.0],
                        tileMode: TileMode.clamp
                    ),
                  ),

        padding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        child: new LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constrains){
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: constrains.maxHeight
                  ),
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        new Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: new TextField(
                            controller: ciController,
                            style: style,
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                hintText: "Carnet de Identidad",
                                errorText: _validate1 ? null: 'Ingresar el carnet de identidad',
                                border:
                                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                          ),
                        ),

                        new Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: new TextField(
                            controller: passController,
                            obscureText: true,
                            style: style,
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                hintText: "Password",
                                errorText: _validate1 ? null : 'Ingresar la contraseña',
                                border:
                                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                          ),
                        ),

                        new Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: new MaterialButton(
                            minWidth: MediaQuery.of(context).size.width,
                            onPressed: () {
                                var auth=new AuthAPIs();
                                auth.doLogin(context, ciController.text, passController.text);
                                setState(() {
                                 getToken(); 
                                });
                              },
                            child: Text("Login",
                                textAlign: TextAlign.center,
                                style: style.copyWith(
                                    color: Colors.white, fontWeight: FontWeight.bold)),
                          ),
                        ),

                        new Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: new MaterialButton(
                            minWidth: MediaQuery.of(context).size.width,
                            onPressed: () {
                                setState(() {
                                    //_usuarioAPI = new UsuariosAPIs().updateUser();
                                    Navigator.push(context,
                                      MaterialPageRoute(
                                      builder: (context)=> Register()
                                      )
                                      );
                                  });
                              },
                            child: Text("Register",
                                textAlign: TextAlign.center,
                                style: style.copyWith(
                                    color: Colors.white, fontWeight: FontWeight.bold)),
                          ),
                        ),


                      ],
                    )),
                ),
              );
          }
        ),

      ),
    );
  }
    getToken()async{
      SharedPreferences preferences = await SharedPreferences.getInstance();
      String getToken =preferences.getString("UltimoToken");
      setState(() {
       msg="token: $getToken"; 
      });
    }
    
}