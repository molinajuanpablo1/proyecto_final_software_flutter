import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:proyecto_final_software_flutter/Rutas.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

String username;

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ABM',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: internalRoutes,
    );
  }
}
