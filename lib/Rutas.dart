import 'package:flutter/material.dart';
import 'package:proyecto_final_software_flutter/Vistas/Listado.dart';
import 'package:proyecto_final_software_flutter/Vistas/Login.dart';

final internalRoutes = {
  '/' : (BuildContext context) => Login(),
  '/login' : (BuildContext context) => Login(),
  '/home' : (BuildContext context) => Listado(),
};