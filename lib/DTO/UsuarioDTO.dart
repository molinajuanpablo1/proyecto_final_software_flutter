import 'dart:convert';

class UsuarioDTO{

  String ci, nickName, carrera, apellido_p;
  String email, nombre, rol, apellido_m;
  String foto_perfil;

  UsuarioDTO({this.ci, this.nickName, this.carrera, this.foto_perfil, this.apellido_p, 
            this.apellido_m, this.email, this.nombre, this.rol });
  
  factory UsuarioDTO.fromJson(Map<String, dynamic> json){
    return  new UsuarioDTO(
      ci: json['ci'].toString(),
      nickName: json['nickName'],
      carrera: json['carrera'],
      foto_perfil: json['foto_perfil'],
      apellido_p: json['apellido_p'],
      apellido_m: json['apellido_m'],
      email: json['email'],
      nombre: json['nombre'],
      rol: json['rol'],

    );
  }

  List<UsuarioDTO> todos_usuarios(String str){
    final jsonlist = json.decode(str);
    return new List<UsuarioDTO>.from(jsonlist.map((x) => UsuarioDTO.fromJson(x)));
  }
}