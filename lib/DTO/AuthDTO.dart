import 'UsuarioDTO.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthDTO {

  final String token;
  final UsuarioDTO usuarioDTO;

  AuthDTO({this.token, this.usuarioDTO});

  factory AuthDTO.fromJson(Map<String, dynamic> json){
    return new AuthDTO(
      token: json['token'],
      usuarioDTO: UsuarioDTO.fromJson(json['usuario'])
    );
  }

  Map<String, dynamic> toJson() => {
    'token': token,
    'user': usuarioDTO,
  };


}
  salvarSesion(String token, UsuarioDTO usuario) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('UltimoToken', token);
    await preferences.setString('UltimoCi', usuario.ci);
    await preferences.setString('UltimonickName', usuario.nickName);

  }