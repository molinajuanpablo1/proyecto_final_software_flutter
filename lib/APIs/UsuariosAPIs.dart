import 'package:flutter/src/widgets/editable_text.dart';
import 'package:proyecto_final_software_flutter/DTO/UsuarioDTO.dart';
import 'package:http/http.dart' as http;

class UsuariosAPIs {
  Future<List<UsuarioDTO>> getUsers(String search) async {
     if(search.isEmpty)
    {
      print('object');
      final response = await http.post('http://192.168.0.8:8000/api/usuarios');

      if(response.statusCode == 200)
      {
        var usuarios = new UsuarioDTO();
        return usuarios.todos_usuarios(response.body);
      }
      else{
        print(response.body); 
        throw Exception('Ha ocurrido un error ${response.body}');
      }
    }
    
    else
    {
      final response = await http.post('http://192.168.0.8:8000/api/usuarios',
        body: {
          'ci': search
        }
      );
      if(response.statusCode == 200){
        var usuarios = new UsuarioDTO();
        return usuarios.todos_usuarios(response.body);
      }else{
        throw Exception('Ha ocurrido un error ${response.body}');
      }
    }
  }

  Future<List<UsuarioDTO>> getUser(String usuario) async{
    final response = await http.get('http://localhost:8000/api/usuarios/$usuario');
    if (response.statusCode == 200){
      var usuarios = new UsuarioDTO();
      return usuarios.todos_usuarios(response.body);
    }else{
      throw Exception('Error al cargar los usuarios ${response.body}');
    }
  }

  Future<List<UsuarioDTO>> deleteUser(String ci) async{
    final response = await http.delete('http://192.168.0.8:8000/api/usuarios/$ci');
    if (response.statusCode == 200){
      var usuarios = new UsuarioDTO();
    }else{
      throw Exception('Error al cargar los usuarios ');
    }
  }

  Future<List<UsuarioDTO>> updateUser(String ciController, String nickNameController, String carreraController) async{
    final response = await http.put('http://192.168.0.8:8000/api/usuarios',
    body:{
      "ci": ciController,
      "nickName": nickNameController,
      "carrera": carreraController
    });
    if (response.statusCode == 200){
      
    }else{
      throw Exception('Error al modificar los usuarios ');
    }
  }
  Future<List<UsuarioDTO>> createUser(String ciController, String passController, String nickNameController, String carreraController, String apellido_pController, String apellido_mController, String emailController, String nombreController, String rolController) async{
    final response = await http.post('http://192.168.0.8:8000/api/usuariosc',
    body:{
    	"ci": ciController,
      "password": passController,
      "nickName": nickNameController,
      "carrera": carreraController,
      "apellido_p": apellido_pController,
      "apellido_m": apellido_mController,
      "email": emailController,
      "nombre": nombreController,
      "rol": rolController

    });
    if (response.statusCode == 200){
      
    }else{
      throw Exception('Error al modificar los usuarios ');
    }
  }
}