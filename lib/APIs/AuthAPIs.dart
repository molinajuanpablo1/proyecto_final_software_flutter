import 'dart:convert';
import 'package:proyecto_final_software_flutter/DTO/AuthDTO.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/widgets.dart';
import 'package:proyecto_final_software_flutter/DTO/UsuarioDTO.dart';

class AuthAPIs{
  doLogin(BuildContext context, String usuario, String password ) async{
    final response = await http.post('http://192.168.0.8:8000/api/auth/login',
    body: {
      'ci': usuario,
      'password': password,
    }
    );
    if(response.statusCode == 200){
      final respuesta= json.decode(response.body);
      var auth= new AuthDTO.fromJson(respuesta);
      UsuarioDTO usuario = auth.usuarioDTO;
      String token = auth.token;
      salvarSesion(token, usuario);
      Navigator.of(context).pushReplacementNamed('/home');
    }else{
      salvarSesion("", new UsuarioDTO(ci: "empty", nickName: "empty" ));
    }

  }
}